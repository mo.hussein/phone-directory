<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<%@ page import = "java.util.*" %>
<html>
<head>
	<title>User Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a User
</h1>

<c:url var="addAction" value="/user/add" ></c:url>

<form:form action="${addAction}" commandName="user">
<table>
	<c:if test="${!empty user.name}">
	<tr>
		<td>
			<form:label path="id">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="id" readonly="true" size="8"  disabled="true" />
			<form:hidden path="id" />
		</td> 
	</tr>
	</c:if>
	<tr>
		<td>
			<form:label path="name">
				<spring:message text="Name"/>
			</form:label>
		</td>
		<td>
			<form:input path="name" />
		</td> 
		
		<td>
			<form:label path="surname">
				<spring:message text="Surname"/>
			</form:label>
		</td>
		<td>
			<form:input path="surname" />
		</td> 
		
		<td>
			<form:label path="gender">
				<spring:message text="gender"/>
			</form:label>
		</td>
		<td>
			<form:input path="gender" />
			
		</td> 
		
		<td>
			<form:label path="dateOfBirth">
				<spring:message text="Date of Birth(YYYY-MM-DD)"/>
			</form:label>
		</td>
		<td>
			<form:input type="date" path="dateOfBirth" />
		</td> 
	</tr>
	
	
	<tr>
		<td colspan="2">
			<c:if test="${!empty user.name}">
				<input type="submit"
					value="<spring:message text="Edit User"/>" />
			</c:if>
			<c:if test="${empty user.name}">
				<input type="submit"
					value="<spring:message text="Add User"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<br>
<h3>Users List</h3>
<c:if test="${!empty listUsers}">
	<table class="tg">
	<tr>
		<th width="80">User ID</th>
		<th width="120">User Name</th>
		<th width="120">User Surname</th>
		<th width="120">User Gender</th>
		<th width="120">Date of Birth</th>
		<th width="120">Age</th>
		<th width="120">Phones</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
		<th width="60">Add Phone</th>
	</tr>
	<c:forEach items="${listUsers}" var="user">
		<tr>
			<td>${user.id}</td>
			<td>${user.name}</td>
			<td>${user.surname}</td>
			<td>${user.gender}</td>
			<td>${user.dateOfBirth}</td>
			<td>${user.age}</td>
			
			<td>
			<c:if test="${!empty user.phones}">
				<table>
					<th width="60">Number</th>
					<th width="60">Action</th>
				
				<c:forEach items="${user.phones}" var="u">
					<tr>
						<td>${u.number}</td>
						<td>
						<a href="<c:url value='/phone/remove/${u.id}' />" >Delete</a>
						</td>
					</tr>
		<c:set var="i" value="${i+1}" />
	</c:forEach>
	</table>
	</c:if>
	
	</td>
			
			<td><a href="<c:url value='/edit/${user.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/remove/${user.id}' />" >Delete</a></td>
			<td> <a href="phone/?userID=${user.id}" >Add</a> </td>
		</tr>
	</c:forEach>
	</table>
</c:if>
</body>
</html>
