package com.spring.dao;

import java.util.List;

import com.spring.model.Phone;

public interface PhoneDAO {

	public void addPhone(Phone p);
	public void updatePhones(Phone p);
	public List<Phone> listPhones();
	public Phone getPhoneById(int id);
	public void removePhone(int id);
}
