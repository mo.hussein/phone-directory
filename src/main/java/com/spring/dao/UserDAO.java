package com.spring.dao;

import java.util.List;

import com.spring.model.User;

public interface UserDAO {

	public void addUser(User u);
	public void updateUser(User p);
	public List<User> listUsers();
	public User getUserById(int id);
	public void removeUser(int id);
}
