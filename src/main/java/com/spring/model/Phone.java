
package com.spring.model;

import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import com.google.i18n.*;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonemetadata.NumberFormat;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.data.*;
import com.google.i18n.phonenumbers.ShortNumbersRegionCodeSet;


@Entity
@Table(name="PHONE")
public class Phone {
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	

	
	public String number;
	
	private String country;
	
	/*
	 private int phonNumber;
	

	public int getPhoneNumber() {
		
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		try {
		    // phone must begin with '+'
		    PhoneNumber numberProto = phoneUtil.parse(number, "");
		    int countryCode = numberProto.getCountryCode();
		} catch (NumberParseException e) {
		    System.err.println("NumberParseException was thrown: " + e.toString());
		}
		
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		int countryCode = phoneUtil.getCountryCodeForRegion("GB");
		phonNumber = countryCode;
		return phonNumber;
	}
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	  private User user;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	


	@Override
	public String toString(){
		
		return "id="+id+", number="+number+", country="+country;
	}
}
