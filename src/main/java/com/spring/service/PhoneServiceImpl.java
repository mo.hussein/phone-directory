package com.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.spring.dao.PhoneDAO;
import com.spring.model.Phone;

@Service
public class PhoneServiceImpl implements PhoneService {
	
	private PhoneDAO phoneDAO;

	public void setPhoneDAO(PhoneDAO phoneDAO) {
		this.phoneDAO = phoneDAO;
	}

	@Override
	@Transactional
	public void addPhone(Phone p) {
		this.phoneDAO.addPhone(p);
	}

	@Override
	@Transactional
	public void updatePhone(Phone p) {
		this.phoneDAO.updatePhones(p);
	}

	@Override
	@Transactional
	public List<Phone> listPhones() {
		return this.phoneDAO.listPhones();
	}

	@Override
	@Transactional
	public Phone getPhoneById(int id) {
		return this.phoneDAO.getPhoneById(id);
	}

	@Override
	@Transactional
	public void removePhone(int id) {
		this.phoneDAO.removePhone(id);
	}

}
