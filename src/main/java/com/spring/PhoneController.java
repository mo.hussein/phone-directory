package com.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.spring.model.Phone;
import com.spring.service.PhoneService;


@Controller
public class PhoneController {
	
	private PhoneService phoneService;
	
	@Autowired(required=true)
	@Qualifier(value="phoneService")
	public void setPhoneService(PhoneService ps){
		this.phoneService = ps;
	}
	
	@RequestMapping(value = "/phone", method = RequestMethod.GET)
	public String addUserPhone(Model model) {
		model.addAttribute("phone", new Phone());
		return "phone";
	}
	
	//For add and update phone both
	@RequestMapping(value= "/phone/add", method = RequestMethod.POST)
	public String addPhone(@ModelAttribute("phone") Phone p){
		
		if(p.getId() == 0){
			//new phone, add it
		
			if  (p.number.charAt(0) != '+')
			{
				PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
				int countryCode = phoneUtil.getCountryCodeForRegion(p.getCountry());
				String num = p.number;
				p.number ="+" + Integer.toString(countryCode)+num;
			}
				
			
			this.phoneService.addPhone(p);
		}else{
			//existing phone, call update
			this.phoneService.updatePhone(p);
		}
		
		return "redirect:/users";
		
	}
	
	@RequestMapping("/phone/remove/{id}")
    public String removePhone(@PathVariable("id") int id){
		
        this.phoneService.removePhone(id);
        return "redirect:/users";
    }
 
    @RequestMapping("/phone/edit/{id}")
    public String editPhone(@PathVariable("id") int id, Model model){
        model.addAttribute("phone", this.phoneService.getPhoneById(id));
        model.addAttribute("listPhones", this.phoneService.listPhones());
        return "phone";
    }
	
}
