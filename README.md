# Phone Directory 
This is Java Spring App, built with Maven, and Hibernate Framework as ORM tool.

This App helps you to add users, and add several mobile numbers to each user.

You can add mobile number in local format or international format, the local format will be added to database in international format.
Google **libphonenumber** library used to get country number code from entered ISO 3166-1 two letter country code.

# Getting Started
You can find sql file of the database in Database Example folder, import this file into mysql database.

Change database name and mysql (username, passwrod) from src-> WEB-INF-> spring-> appServlet-> servlet-context.xml (Line 32).

*/users* is the starting route which navigate to the main page used for this application.


## Screenshots

https://ibb.co/GMCMvz7

https://ibb.co/nmmv7YM